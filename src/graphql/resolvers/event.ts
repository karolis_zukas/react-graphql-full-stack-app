import Event, { IEvent } from '../../models/event';
import User from '../../models/user';
import { user } from './user';

export const events = (eventIds: string[]) => {
    return Event.find({_id: { $in: eventIds }})
        .then(events => events.map(event => {
            return {
                ...event,
                creator: user(event.creator as string )}
        })).catch(err => {
            throw err;
        })
}

const eventResolvers = {
    events: async (): Promise<IEvent[]> => {
        try {
            const events = await Event.find().populate('creator');
            return events;
        } catch (err) {
            // TODO: add error logging
            return err;
        }
    },
    createEvent: async (args: { eventInput: IEvent }): Promise<IEvent> => {

        const event = new Event({
            title: args.eventInput.title,
            description: args.eventInput.description,
            price: +args.eventInput.price,
            date: new Date(args.eventInput.date),
            creator:'5e89f454f7d605ebb8898eb6'
        });

        try {
            const storedEvent = await event.save();
            const user = await User.findById('5e89f454f7d605ebb8898eb6') as any;
            if(!user) {
                throw new Error('User not found');
            }
            user.createdEvents.push(event);
            user.save();

            return storedEvent;
        } catch (err) {
            // TODO: add error logging
            return err;
        }
    }
};

export default eventResolvers;