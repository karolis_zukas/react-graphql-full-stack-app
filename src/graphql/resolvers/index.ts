import userResolvers from './user';
import eventResolvers from './event';

export {userResolvers, eventResolvers };