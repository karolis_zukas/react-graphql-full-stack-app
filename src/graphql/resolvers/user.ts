import bcrypt from 'bcryptjs';
import User, { IUser } from '../../models/user';
import { events } from './event';

export const user = (userId: string): any => {
    return User.findById(userId)
        .then((user: IUser | null) => {
            if (!user) {
                throw new Error('No user found');
            }
            return {
                ...user,
                password: null,
                createdEvents: events(user.createdEvents)
            }
        }).catch(err => {
            throw err;
        })
}

const userResolvers = {
    createUser: async(args: { userInput: IUser }): Promise<{ email: string, _id: string }> => {
        try {
            let user = await User.findOne({email: args.userInput.email});
            if (user) {
                throw new Error('User already exists');
            }
            const hashedPassword = await bcrypt.hash(args.userInput.password as string, 12);
            user = new User({ email: args.userInput.email, password: hashedPassword });
            const result = await user.save();

            return { email: result.email, _id: result.id };
        } catch (err) {
            // TODO: add error logging
            return err;
        }
    }
};

export default userResolvers;