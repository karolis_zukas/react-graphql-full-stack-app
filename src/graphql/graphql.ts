import { buildSchema } from 'graphql';
import { eventResolvers, userResolvers } from './resolvers';

const schema = buildSchema(`
    type Event {
        _id: ID!
        title: String!
        description: String!
        price: Float!
        date: String!
        creator: User!
    }

    type User {
        _id: ID!
        email: String!
        password: String
        createdEvents: [Event!]
    }

    input EventInput {
        title: String!
        description: String!
        price: Float!
        date: String!
    }

    input UserInput {
        email: String!
        password: String!
    }

    type RootQuery {
        events: [Event!]!
    }

    type RootMutation {
        createEvent(eventInput: EventInput): Event
        createUser(userInput: UserInput): User
    }

    schema {
        query: RootQuery
        mutation: RootMutation
    }
`);

const combinedResolvers = {
    ...eventResolvers,
    ...userResolvers
};

export const graphqlConfig = {
    schema: schema,
    rootValue: combinedResolvers,
    graphiql: true
};
