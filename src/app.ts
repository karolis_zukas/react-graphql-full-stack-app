import express from 'express';
import bodyParser from 'body-parser';
import graphqlHttp from 'express-graphql';
import { graphqlConfig } from './graphql/graphql';
import mongoose from 'mongoose';

const app = express();
app.use(bodyParser.json());

app.use('/api', graphqlHttp(graphqlConfig));

mongoose.connect(
    `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster1-zhuoy.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        app.listen(3000);
        console.log(`Server successfully started!`);
    }).catch(err => {
        console.log(`Mongo DB error ${err}`)
    });