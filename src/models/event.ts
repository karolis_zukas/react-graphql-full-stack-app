import mongoose, { Schema, Document, Model } from 'mongoose';
import { IUser } from './user';

export interface IEvent extends Document {
    _id: string;
    title: string;
    description: string;
    price: number;
    date: string;
    creator: string | IUser;
}

export interface IEventModel extends Model<IEvent> {
    // Can extend mongoose model methods, with conctinated read and so on.
}

const EventSchema: Schema = new Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    price: { type: Number, required: true },
    date: { type: Date, required: true },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

export default mongoose.model<IEvent, IEventModel>('Event', EventSchema);