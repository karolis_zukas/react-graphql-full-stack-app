import mongoose, { Schema, Document, Model } from 'mongoose';
import { IEvent } from './event';

export interface IUser extends Document {
    _id: string;
    email: string;
    password: string | null;
    createdEvents: string[]
};

export interface IUserModel extends Model<IUser> {
    // Can extend mongoose model methods, with conctinated read and so on.
}

const UserSchema: Schema = new Schema({
    email: { type: String, required: true },
    password: { type: String, required: true },
    createdEvents: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Event'
        }
    ]
});

export default mongoose.model<IUser, IUserModel>('User', UserSchema);